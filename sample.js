function showPreloader() {
    var preloader = '<div class="preloader"><img src="images/loading.gif" alt=""></div>';

    $('body').addClass('no-scroll').append(preloader);
}

function hidePreloader() {
    $('.preloader').remove();
    $('body').removeClass('no-scroll');
}

jQuery.fn.highlight = function(pat) {
    function innerHighlight(node, pat) {
        var skip = 0;
        if (node.nodeType == 3) {
            var pos = node.data.toUpperCase().indexOf(pat);
            if (pos >= 0) {
                var spannode = document.createElement('span');
                spannode.className = 'highlight';
                var middlebit = node.splitText(pos);
                var endbit = middlebit.splitText(pat.length);
                var middleclone = middlebit.cloneNode(true);
                spannode.appendChild(middleclone);
                middlebit.parentNode.replaceChild(spannode, middlebit);
                skip = 1;
            }
        } else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
            for (var i = 0; i < node.childNodes.length; ++i) {
                i += innerHighlight(node.childNodes[i], pat);
            }
        }
        return skip;
    }
    return this.length && pat && pat.length ? this.each(function() {
            innerHighlight(this, pat.toUpperCase());
        }) : this;
};

function showMainNav() {
    var mainNav = $('#mainNav'),
        body = $('body');

    $('#mainNavTrigger').on('click', function (e) {
        e.preventDefault();
        if(!mainNav.hasClass('open')) {
            body.addClass('no-scroll');
            mainNav.addClass('open');
            closeClickedOverlay(closeMainNav);
            dragMenu();
        } else {
            closeMainNav();
        }
    });
}

function closeMainNav() {
    var mainNav = $('#mainNav'),
        body = $('body');

    body.removeClass('no-scroll');
    mainNav.removeClass('open');

    return false;
}

function closeClickedOverlay(callback) {
    var overlay = $('.overlay');

    overlay.each(function () {
        $(this).on('click', function () {
            callback();
        });
    });
}

function dragMenu() {
    var pageWrapper = document.querySelector('.page-wrapper'),
        mainNavCnt = document.getElementById('mainNav');

    /*Ловим касание*/
    mainNavCnt.addEventListener('touchstart', function(event) {
        if (event.targetTouches.length == 1) {
            var touch=event.targetTouches[0];
            touchOffsetX = touch.pageX - touch.target.offsetLeft;
            touchOffsetY = touch.pageY - touch.target.offsetTop;
        }
    }, false);
    mainNavCnt.addEventListener('touchmove', function(event) {
        if (event.targetTouches.length == 1) {
            var touch = event.targetTouches[0];
            if(touchOffsetX - touch.pageX > 100) {
                closeMainNav();
            }
        }
    }, false);

    mainNavCnt.onmousedown = function(e) {
        var navCoords = getCoords(mainNavCnt);
        var shiftX = e.pageX - navCoords.left;

        var pageWrapperCoords = getCoords(pageWrapper);

        document.onmousemove = function(e) {
            var newLeft = e.pageX - shiftX - pageWrapperCoords.left;

            var rightEdge = pageWrapper.offsetWidth - mainNavCnt.offsetWidth;

            if (newLeft > rightEdge) {
                newLeft = rightEdge / 2;
            }

            // mainNavCnt.style.left = newLeft + 'px';

            if(newLeft < -150) {
                closeMainNav();
            }

            document.onmouseup = function() {
                document.onmousemove = document.onmouseup = null;
            };
        };
        return false;
    };

    mainNavCnt.ondragstart = function() {
        return false;
    }
}


function getCoords(elem) { // кроме IE8-
    var box = elem.getBoundingClientRect();

    return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset
    };

}

function showUserMenu() {
    var userMenu = $('#userMenu');

    $('#userMenuTrigger').on('click', function (e) {
        e.preventDefault();
        if(!userMenu.hasClass('opened')) {
            userMenu.addClass('opened');
            setCookie('userMenuOpened', true);
        } else {
            userMenu.removeClass('opened');
            setCookie('userMenuOpened', false);
        }
    });
}


function checkUserMenuCookies() {
    var userMenu = $('#userMenu');

    if(getCookie('userMenuOpened') == 'true') {
        userMenu.addClass('opened');
    } else {
        userMenu.removeClass('opened');
    }
}


function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function checkElementExistence(selector, fn) {
    var item = $(selector);
    if(item) {
        fn();
    }
}

function activateScrollHeader() {
    var mainHeader = $('.header');

    var scrolling = false,
        previousTop = 0,
        currentTop = 0,
        scrollDelta = 10,
        scrollOffset = 150;

    $(window).on('scroll', function(){
        if( !scrolling ) {
            scrolling = true;
            (!window.requestAnimationFrame)
                ? setTimeout(autoHideHeader, 250)
                : requestAnimationFrame(autoHideHeader);
        }
    });

    function autoHideHeader() {
        var currentTop = $(window).scrollTop();

        checkSimpleNavigation(currentTop);

        previousTop = currentTop;
        scrolling = false;
    }

    function checkSimpleNavigation(currentTop) {
        //there's no secondary nav or secondary nav is below primary nav
        if (previousTop - currentTop > scrollDelta) {
            //if scrolling up...
            mainHeader.removeClass('is-hidden');
        } else if( currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
            //if scrolling down...
            mainHeader.addClass('is-hidden');
        }
    }
}

function lotItemMouseOverAction() {
    var lotItem = $('.lot-item');

    lotItem.each(function(idx, el){
        var item = $(el);

        item.on('click', function () {
            if(!item.hasClass('active')) {
                clearAllLots();
                item.addClass('active');
            } else {
                item.removeClass('active');
            }
        });
    });
}

function clearAllLots() {
    var lotItem = $('.lot-item');

    lotItem.each(function(idx, el) {
        var item = $(el);
        item.removeClass('active');
    });
}

function activateInput() {
    var formInputsArr = $('.form-input');

    formInputsArr.each(function(index, element) {
        var formInput = $(element),
            input = formInput.find('input');

        if(input.val() !== '' || input.prop('autofocus') || input.hasClass('focus.inputmask')) {
            formInput.addClass('active');
        }

        input.on('focus change keyup paste input', function() {
            formInput.addClass('active');
        });

        input.on('blur', function() {
            if(input.val() == '') {
                formInput.removeClass('active');
            }
            input.val() !== '' ? formInput.addClass('success') :
                formInput.removeClass('success');
        });
    });
}

var searchBox = {
    ajaxReq: null,
    currValue: '',
    init: function(elem) {
        var self = this,
            $wrap = elem,
            $input = $wrap.find('.form-input__input'),
            $submit = $wrap.find('.search-box__btn'),
            $reset = $wrap.find('.search-box__btn-clear'),
            $helper = $wrap.find('.completer');

        $input.on('focus', function() {
            $wrap.addClass('active');
            self.inputCheck($.trim(this.value), $helper);
        }).on('blur', function() {
            setTimeout(function() {
                $wrap.removeClass('active');
                $helper.removeClass('active');
            }, 200);
        }).on('keyup', function(e) {
            if (e.keyCode !== 38 && e.keyCode !== 40) {
                self.inputCheck($.trim(this.value), $helper);
            } else {
                e.preventDefault();
            }
        }).on('keydown', function(e) {
            if (e.keyCode === 13) {
                $wrap.find('form').submit();
                return;
            }
            if (e.keyCode !== 38 && e.keyCode !== 40) {
                $helper.find('.active').removeClass('active');
                return;
            }
            e.preventDefault();
            if ($helper.find('.queries').is(":visible")) {
                var li = $helper.find('li');
            } else {
                var li = $helper.find('.results li');
            }
            var curr = null;
            li.each(function(ind) {
                if ($(this).hasClass('active')) {
                    curr = ind;
                }
            });
            if (e.keyCode === 38) {
                if (curr !== null) {
                    li.eq(curr).removeClass('active');
                    curr--;
                }
                if (curr === null || curr < 0) {
                    curr = li.length - 1;
                }
                li.eq(curr).addClass('active');
            } else if (e.keyCode === 40) {
                if (curr !== null) {
                    li.eq(curr).removeClass('active');
                    curr++;
                }
                if (curr === null || curr === li.length) {
                    curr = 0;
                }
                li.eq(curr).addClass('active');
            }
            var resultText = li.eq(curr).find('.t').length ? li.eq(curr).find('.t').text() : li.eq(curr).text();
            if (resultText) {
                $input.val(resultText);
            }
        });
        $reset.on('click', function() {
            $input.val('').blur();
        });
        $helper.find('.results').on('click', 'li', function() {
            $input.val($(this).find('.t').text()).focus();
            $wrap.find('form').submit()
        });
    },
    inputCheck: function(val, helper) {
        val = val || '';
        if (val.length > 1) {
            helper.addClass('active');
            this.currValue = val;
            this.helperRequest(val, helper);
        } else {
            helper.find('.results').html('');
            helper.find('.queries').show();
            helper.removeClass('active');
        }
        if (helper.find('.queries').length) {
            helper.addClass('active');
        }
    },
    helperRequest: function(val, helper) {
        var self = this,
            str = 'Ноутбук Lenovo M3650\nНоутбук HP ХS 289 (с деффектами)\nНоутбук ASUS G1823-32 (16 Gb, 500 Gb)\nНоутбук DELL (на запчасти)\nНоутбук ACER. Подробности по телефону';
        if (this.ajaxReq) {
            this.ajaxReq.abort();
        }

        this.ajaxReq = $.ajax({
            url: '123',
            type: 'post',
            data: {
                query: val
            },
            success: function(response) {
                self.helperRequestHandler(response, helper);
            },
            error: function() {
                //Для примера
              self.helperRequestHandler(str, helper);
            }
        });
    },
    helperRequestHandler: function(response, helper) {
        var results = response.split('\n');
        var html = '';
        if (results.length) {
            for (var i = 0; i < results.length; i++) {
                if (!results[i].length) {
                    continue;
                }
                var parts = results[i].split('|');
                html += '<li><span class="t">' + parts[0] + '</span>' + (parts.length === 2 ? ' <span class="c">в разделе ' + parts[1] + '</span>' : '') + '</li>';
            }
        }
        if (html.length) {
            html = '<ul>' + html + '</ul>';
            helper.find('.results').html(html).highlight(this.currValue);
            helper.find('.queries').hide();
        } else {
            helper.find('.results').html('');
        }
    }
};

var citySearchBox = {
    ajaxReq: null,
    currValue: '',
    defautCitiesList: [
        {
            name: 'Бобруйск',
            href: 'bobruisk'
        },
        {
            name: 'Борисов',
            href: 'borisov'
        },
        {
            name: 'Витебск',
            href: 'vitebsk'
        },
        {
            name: 'Гомель',
            href: 'gomel'
        },
        {
            name: 'Минск',
            href: 'minsk'
        }
    ],
    init: function(elem) {
        var self = this,
            $wrap = elem,
            $input = $wrap.find('.form-input__input'),
            $helper = $wrap.find('.completer'),
            $form = $wrap.find('form');

        $form.on('submit', function (e) {
            e.preventDefault();
        });
        $input.on('keyup', function(e) {
            self.inputCheck($.trim(this.value), $helper);
        });
    },
    inputCheck: function(val, helper) {
        val = val || '';
        if (val.length > 1) {
            helper.addClass('active');
            this.currValue = val;
            this.helperRequest(val, helper);
        } else {
            helper.find('.results').html('');
            helper.find('.queries').show();
            helper.removeClass('active');
        }
        if (helper.find('.queries').length) {
            helper.addClass('active');
        }
    },
    helperRequest: function(val, helper) {
        var self = this;
        var str = 'sadfasdf\nasdasdasd\nasdasdzzvxcvx\nloper';
        var defaultCitiesList = this.defautCitiesList;

        if (this.ajaxReq) {
            this.ajaxReq.abort();
        }
        this.ajaxReq = $.ajax({
            url: '234',
            type: 'post',
            data: {
                query: val
            },
            success: function(response) {
                self.helperRequestHandler(response, helper);
            },
            error: function() {
                self.helperRequestHandler(defaultCitiesList, helper);
            }
        });
    },
    helperRequestHandler: function(response, helper) {
        var results = response;
        var html = '';
        if (results.length) {
            var resultsScope = '';
            for (var i = 0; i < results.length; i++) {
                var cityName = results[i].name,
                    cityHref = results[i].href;
                if (!cityName) {
                    continue;
                }
                if(cityName.toLowerCase().indexOf(this.currValue.toLowerCase()) !== -1) {
                    console.log(cityName);
                    html += '<li><a class="city" href="'+cityHref+'">'+cityName+'</a></li>';
                }
                resultsScope += cityName.toLowerCase();
            }
        }
        if(resultsScope.indexOf(this.currValue.toLowerCase()) == -1) {
            html += '<li><span class="t">Город не найден</span></li>';
        }
        if (html.length) {
            html = '<ul>' + html + '</ul>';
            helper.find('.results').html(html).highlight(this.currValue);
            helper.find('.queries').hide();
        } else {
            helper.find('.results').html('');
        }
    }
};

var popupwin = {
    load: function(url, post) {
        var loadingtimer = setTimeout(function() {
            $.fancybox.showLoading();
        }, 1000);
        $.ajax({
            url: url,
            type: 'post',
            data: post,
            success: function(response) {
                if (typeof loadingtimer != 'undefined') {
                    clearTimeout(loadingtimer);
                    $.fancybox.hideLoading();
                }
                if (response) {
                    $('#popupbox').html(response);
                    $('.popup .ico-close,.popup .form-button.cancel').on('click', function(e) {
                        e.preventDefault();
                        popupwin.hide();
                    });
                    popupwin.show();
                }
            }
        });
    },
    show: function(selector) {
        var self = this,
            item = $('<div>').append($(selector).clone()).html(),
            itemStorage = $(selector).remove();

        $('body').addClass('popup-opened');
        $('.popup-overlay').addClass('opened');
        $('.popup__cnt').html(item);

        var $elem = $('#popupbox'),
            $close = $elem.find('.popup__close'),
            $cancel = $elem.find('.cancel');

        $elem.addClass('opened');

        $close.on('click', function (e) {
            e.preventDefault();
            closeActions();
        });

        $cancel.each(function() {
            $(this).on('click', function (e) {
                e.preventDefault();
                closeActions();
            })
        });

        function closeActions() {
            self.hide();
            self.reestablish(itemStorage);
        }
        activateInput();
    },
    hide: function() {
        $('body').removeClass('popup-opened');
        $('.popup,.popup-overlay').removeClass('opened');
        $('.popup__cnt').html('');
    },
    reestablish: function (item) {
        $('#allPopupsWrapper').append(item);
    }
};

function changeAvatar(input, output) {
    var inputFile = $(input),
        files = inputFile[0].files,
        output = $('#' + output),
        deleteUploadedFile = $('.delete-uploaded-file'),
        fileName;

    for (var i = 0, f; f = files[i]; i++) {

        // Only process image files.
        if (!f.type.match('image.*')) {
            continue;
        }

        var reader = new FileReader();

        reader.onload = (function (theFile) {
            return function (e) {
                output.attr('src', e.target.result);
            };
        })(f);

        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
    }
}

function resetAvatar(item) {
    var img = $('#'+item);

    img.attr('src', 'images/users/5.jpg');
}

function showPopup(tgSelector, popupSelector) {
    $(tgSelector).on('click', function (e) {
        e.preventDefault();
        popupwin.show(popupSelector);
        closeMainNav();
    });
}

function addPicture() {
    var $addPictureBtn = $('#addPictureBtn'),
        $addPictureBtnParent = $addPictureBtn.parents('.item'),
        files = $addPictureBtn[0].files,
        img = document.createElement('img'),
        div= document.createElement('div'),
        fileName;

    for (var i = 0, f; f = files[i]; i++) {

        // Only process image files.
        if (!f.type.match('image.*')) {
            continue;
        }

        var reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (function (theFile) {
            return function (e) {
                $(img).attr('src', e.target.result);
                $(div).addClass('item').append(img);
                $addPictureBtnParent.before(div);
            };
        })(f);

        reader.readAsDataURL(f);
    }
}

function checkPopupTriggerExistence(tgSelector, popupSelector, fn) {
    var item = $(tgSelector);
    if(item) {
        fn(tgSelector, popupSelector);
    }
}

function showCategorySearchPopup() {
    $('#searchTg').on('click', function (e) {
        e.preventDefault();
        popupwin.show('#searchCategoryPopup');
        searchBox.init($('#searchCategoryPopup'));
    });
}

function showCitySearchPopup() {
    $('#searchCityTg').on('click', function (e) {
        e.preventDefault();
        popupwin.show('#searchCityPopup');
        citySearchBox.init($('#searchCityPopup'));
    });
}

function showChatPopup() {
    $('#chatTg').on('click', function (e) {
        e.preventDefault();

        popupwin.show('#chatContactsPopup');

        showConversationPopup();
        closeMainNav();
    });

    function showConversationPopup() {
        $('[data-type="contact-name"]').each(function () {
            $(this).on('click', function (e) {
                e.preventDefault();
                var name = $(this).text();

                popupwin.show('#chatConversationPopup');

                $('#interlocutorName').html(name);

                $('#backToContactsTg').on('click', function () {
                    popupwin.show('#chatContactsPopup');
                    showConversationPopup();
                })
            });
        });
    }
}

function showMsgMenuList(ctx) {
    var item = $(ctx),
        parent = item.parents('.msg-menu');

    parent.addClass('show');
    hideClickedOut(parent[0]);

}

function hideClickedOut(item) {
    var block = $(item);

    $(document).on('click', function (e) {
        if (block.has(e.target).length === 0){
            block.removeClass('show');
        }
    });
}

function activateCarousel() {
    var owl = $('.owl-carousel');

    if(owl[0]) {
        owl.owlCarousel({
            loop:false,
            margin:0,
            mouseDrag: false,
            items: 1,
            dots: true,
            onInitialized: showImageView
        });
    } else return false;


    function showImageView() {
        $('.image-viewer').each(function () {
            $(this).css({'overflow': 'visible'})
        });
    }
}


checkPopupTriggerExistence('#loginId', '#loginPopup', showPopup);
checkPopupTriggerExistence('#searchTg', '#searchCategoryPopup', showCategorySearchPopup);
checkPopupTriggerExistence('#searchCityTg', '#searchCityPopup', showCitySearchPopup);
checkPopupTriggerExistence('#purseTg', '#pursePopup', showPopup);
checkPopupTriggerExistence('#chatTg', '#chatContactsPopup', showChatPopup);
checkPopupTriggerExistence('#userDataTg', '#userDataPopup', showPopup);
checkPopupTriggerExistence('#addLotTg', '#addLotPopup', showPopup);
checkPopupTriggerExistence('#filterTg', '#filterPopup', showPopup);
checkPopupTriggerExistence('#makeOfferTg', '#makeOfferPopup', showPopup);
checkPopupTriggerExistence('#participantTg', '#participantPopup', showPopup);



//Инициализация
activateScrollHeader();

checkUserMenuCookies();

checkElementExistence('#userMenuTrigger', showUserMenu);

checkElementExistence('#mainNav', showMainNav);

lotItemMouseOverAction();

activateInput();

activateCarousel();


// Вставка Svg спрайта и сохрание его в LocalStorage
(function (window, document) {
    'use strict';
    var file = 'images/icons/sprite.svg', // путь к файлу спрайта на сервере
        revision = 5;            // версия спрайта
    if (!document.createElementNS || !document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect) return true;
    var isLocalStorage = 'localStorage' in window && window['localStorage'] !== null,
        request,
        data,
        insertIT = function() {
            document.body.insertAdjacentHTML('afterbegin', data);
        },
        insert = function() {
            if (document.body) insertIT();
            else document.addEventListener('DOMContentLoaded', insertIT);
        };
    if (isLocalStorage && localStorage.getItem('inlineSVGrev') == revision) {
        data = localStorage.getItem('inlineSVGdata');
        if (data) {
            insert();
            return true;
        }
    }
    try {
        request = new XMLHttpRequest();
        request.open('GET', file, true);
        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                data = request.responseText;
                insert();
                if (isLocalStorage) {
                    localStorage.setItem('inlineSVGdata', data);
                    localStorage.setItem('inlineSVGrev', revision);
                }
            }
        };
        request.send();
    } catch (e) {}
}(window, document));







